from fastapi import FastAPI
from pydantic import BaseModel
from llama_index.llms.ollama import Ollama

app = FastAPI()
# Set the model of your liking below
model_name = "mistral"


class CompletionReq(BaseModel):
    prompt: str


class CompletionRes(BaseModel):
    completion: str
    model: str


@app.post("/v1/complete")
def generate(req_body: CompletionReq) -> CompletionRes:
    llm = Ollama(model=model_name)
    prompt = req_body.prompt
    response = llm.complete(prompt)
    return CompletionRes(completion=response.text, model=response.additional_kwargs['model'])


if __name__ == "__main__":
    import uvicorn
    # Make sure to choose a free port for your process
    uvicorn.run(app, host="localhost", port=3030)
