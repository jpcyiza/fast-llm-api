## 🦙 Ollama + LlamaIndex API server

Simple [FastAPI](https://fastapi.tiangolo.com/) setup to serve local AI models [Ollama](https://ollama.com/) and [LlamaIndex](https://docs.llamaindex.ai/en/stable/). This project allows you to easily deploy your AI models locally without the need for cloud services, providing a faster and more private solution.

## Make it run

- Make sure you have [Ollama](https://ollama.com/download) installed.
- Run the mistral model `ollama run mistral` to make sure your model works
   - If you want to update the model to the [0.2](https://ollama.com/library/mistral) version first you can run `ollama pull mistral`
- Create an Python 3.12 environment
- In the environment install the dependencies. Run `pip install -r requirements.txt`
- Run `python main.py`
- Visit `http://localhost:3030/docs` to get to the docs 🚀 

 To make it run from the get go, we running this api with Mistral by default here.
 But you can use any [Ollama model](https://ollama.com/library) you prefer by changing the `model_name` variable in `main.py`.
